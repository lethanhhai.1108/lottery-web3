import React, { useEffect, useState } from 'react'
import { ethers } from "ethers";
import Web3Modal from "web3modal";

import FundMe from "../contracts/FundMe.json";
import contractAddress from "../contracts/fund-me-contract-address.json";
import {Button, Divider, Input, InputNumber} from "antd";

declare var window: any

const Dapp = () => {
  const [errorMessage, setErrorMessage] = useState<any>(null);
  const [defaultAccount, setDefaultAccount] = useState<any>(null);
  const [chainId, setChainId] = useState<any>(null);
  const [connButtonText, setConnButtonText] = useState<any>('Connect Wallet');

  const [currentContractVal, setCurrentContractVal] = useState<any>(null);

  const [provider, setProvider] = useState<any>(null);
  const [signer, setSigner] = useState<any>(null);
  const [contract, setContract] = useState<any>(null);
  const [balance, setBalance] = useState<any>(0);
  const [contractBalance, setContractBalance] = useState<any>(0);
  const [fundValue, setFundValue] = useState<any>(0);

  useEffect(() => {

  }, [])

  const connectWalletHandler = () => {
    if (window.ethereum && window.ethereum.isMetaMask) {

      window.ethereum.request({ method: 'eth_requestAccounts'})
        .then((result: React.SetStateAction<null>[]) => {

          setChainId(window.ethereum.networkVersion)
          accountChangedHandler(result[0]);
          setConnButtonText('Wallet Connected');
        })
        .catch((error: { message: React.SetStateAction<null>; }) => {
          setErrorMessage(error.message);

        });

    } else {
      console.log('Need to install MetaMask');
      setErrorMessage('Please install MetaMask browser extension to interact');
    }
  }

  const disconnectWalletHandler = async () => {
    if (provider?.disconnect && typeof provider.disconnect === 'function') {
      await provider.disconnect()
    }
  }

  // update account, will cause component re-render
  const accountChangedHandler = (newAccount: React.SetStateAction<null>) => {
    setDefaultAccount(newAccount);
    updateEthers(newAccount);
  }

  const chainChangedHandler = () => {
    // reload the page to avoid any errors with chain change mid use of application
    window.location.reload();
  }

  const updateEthers = async (address: any) => {
    let tempProvider = new ethers.providers.Web3Provider(window.ethereum);
    setProvider(tempProvider);

    let tempSigner = tempProvider.getSigner();
    setSigner(tempSigner);

    let tempContract = new ethers.Contract(contractAddress.FundMe, FundMe.abi, tempSigner);
    setContract(tempContract);

    let tempBalance = await tempProvider.getBalance(address)
    const balanceInEth = ethers.utils.formatEther(tempBalance)
    setBalance(balanceInEth);
  }

  // listen for account changes
  window.ethereum.on('accountsChanged', accountChangedHandler);
  window.ethereum.on('chainChanged', chainChangedHandler);

  // Contract
  const getBalance = async () => {
    const total = await contract.total()
    const balanceInEth = ethers.utils.formatEther(total)
    setContractBalance(balanceInEth)
  }

  const fundMe = async () => {
    const fund = await contract.fund({ value: ethers.utils.parseEther(String(fundValue)) })

    const res = await fund.wait()

    console.log(res)
  }

  return (
    <>
      <h3>ChainId: {chainId}</h3>
      <h3>Address: {defaultAccount}</h3>
      <h3>Balance: {balance} ETH</h3>
      <Button onClick={connectWalletHandler}> Connect </Button>
      <Button onClick={disconnectWalletHandler}> Disconnect </Button>
      <Divider />
      <h3>Contract balance: {contractBalance} ETH</h3>
      <Button onClick={getBalance}> Get contract balance </Button>
      <br />
      <br />
      <InputNumber onChange={(val) => {
        setFundValue(val)
      }}/>
      <Button onClick={fundMe}> Fund </Button>
    </>
  )
}

export default Dapp

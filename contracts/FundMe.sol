// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.9.0;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

contract FundMe {
    mapping (address => uint256) public contractToAmountFund;
    address public owner;
    uint256 public total = 0;

    constructor() {
        owner = msg.sender;
    }

    function fund() public payable {
        contractToAmountFund[msg.sender] += msg.value;
        total += msg.value;
    }

    modifier onlyOwner {
        require(owner == msg.sender, "You are not owner!");
        _;
    }

    function withdraw() onlyOwner public payable {

        payable(msg.sender).transfer(address(this).balance);
    }
}
